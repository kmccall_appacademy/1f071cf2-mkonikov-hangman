require 'byebug'
class Hangman
  attr_reader :guesser, :referee, :board

  def initialize(options = {})
    @guesser = options[:guesser]
    @referee = options[:referee]
    @board = []
  end

  def setup
    length = referee.pick_secret_word
    guesser.register_secret_length(length)
    @board = Array.new(length) { '_' }
  end

  def take_turn
    @guess = guesser.guess(@board)
    puts "Thanks for your guess of #{@guess}"
    indices = referee.check_guess(@guess)
    update_board(indices)
    display_board
    guesser.handle_response(@guess, indices)
  end

  def update_board(indices)
    indices.each { |num| @board[num] = @guess} if !indices.nil?
  end

  def display_board
    puts "WORD > #{@board.join}"
  end

  def won?
    @board.none? { |el| el == '_' }
  end

  def play
    setup
    take_turn until won?
    puts "Yay! You won! The word was #{@board.join}"
  end

end

class HumanPlayer
  def initialize(dictionary)
    @dictionary = dictionary
    @used = []
  end

  def pick_secret_word
    puts "Enter your secret word"
    @secret_word = gets.chomp
    @secret_word.length
  end

  def register_secret_length(length)
  end

  def guess(board)
    puts "Guess a letter from the Alphabet"
    gets.chomp
  end

  def check_guess(guess)
    indices = []
    @secret_word.each_char.with_index do |char, index|
      indices << index if char == guess
    end
    indices
  end

  def handle_response(guess, indices)
    @used << guess
  end

end

class ComputerPlayer
  def initialize(dictionary)
    @dictionary = dictionary
    @candidates = dictionary.dup
    @guessed = []
    @board = []
  end

  def most_common
    letters = Hash.new(0)
    @candidates.join.gsub("\n","").each_char do |letter|
      previous_guess = @guessed.include?(letter) || @board.include?(letter)
      letters[letter] += 1 unless previous_guess
    end
    letters = letters.sort_by {|k,v| v}
    letters.last[0]
  end

  def guess(board)
    @board = board
    most_common
  end

  def pick_secret_word
    @secret_word = @dictionary.sample.chomp
    @secret_word.length
  end

  def check_guess(guess)
    (0..@secret_word.length).select do |num|
      @secret_word[num] == guess
    end
  end

  def candidate_words
    @candidates.each do |word|
      break if @indices.nil?
      temp_indices = []
      word.each_char.with_index do |c, index|
        temp_indices << index if c == @guessed.last
      end
      @candidates.delete(word) if temp_indices != @indices
    end
    @candidates
  end

  def handle_response(guess, indices)
    indices.each { |num| @board[num] = guess }
    @indices = indices
    @guessed << guess
    candidate_words
  end

  def register_secret_length(length)
    @candidates.delete_if { |word| (word.chomp.length) != (length) }
  end

end


if __FILE__ == $PROGRAM_NAME
  dictionary = File.readlines("dictionary.txt")
  mendel = HumanPlayer.new(dictionary)
  computer = ComputerPlayer.new(dictionary)
  game = Hangman.new({ referee: mendel, guesser: computer })
  game.play
end
